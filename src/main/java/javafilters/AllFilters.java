package main.java.javafilters;

import java.util.ArrayList;

import main.java.domain.Person;

public class AllFilters {
	
	static ArrayList<IFilter> filters = new ArrayList<IFilter>();

	public AllFilters()
	{
		filters.add(new AgeFilter());
	}
	
	public static void outFilter(ArrayList<Person> people, Person from, Person to)
	{
		for (IFilter filter : filters)
		{
			filter.doFilter(people, from, to);
		}
	}
}
