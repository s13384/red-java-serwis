package main.java.javafilters;
import java.util.ArrayList;

import main.java.domain.*;

public interface IFilter {
	
	public void doFilter(ArrayList<Person> people, Person from, Person to);
}
