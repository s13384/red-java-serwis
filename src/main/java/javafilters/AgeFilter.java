package main.java.javafilters;

import java.util.ArrayList;

import main.java.domain.Person;

public class AgeFilter implements IFilter {

	@Override
	public void doFilter(ArrayList<Person> people, Person from, Person to) {

		for (Person p : people) {
			if (from.getAge() != null) {
				if (p.getAge() < from.getAge()) {
					people.remove(p);
				}
			}
			if (to.getAge() != null) {
				if (p.getAge() > to.getAge()) {
					people.remove(p);
				}
			}
		}

	}

}
