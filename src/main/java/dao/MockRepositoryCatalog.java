package main.java.dao;

import main.java.domain.Person;

public class MockRepositoryCatalog implements RepositoryCatalog{

	@Override
	public Repository<Person> people() {
		return new PersonRepository();
	}

}
