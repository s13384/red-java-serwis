package main.java.dao;

import main.java.domain.Person;

public interface RepositoryCatalog {

	public Repository<Person> people();
}
