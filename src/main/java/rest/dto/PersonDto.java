package main.java.rest.dto;

public class PersonDto extends PersonSummaryDto {
	
	private int age;
	private int weight;
	private String eyeColour;
	private String residence;
	private String education;
	private String description;
	
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public String getEyeColour() {
		return eyeColour;
	}
	public void setEyeColour(String eyeColour) {
		this.eyeColour = eyeColour;
	}
	public String getZamieszkanie() {
		return residence;
	}
	public void setZamieszkanie(String zamieszkanie) {
		this.residence = zamieszkanie;
	}
	public String getWyksztalcenie() {
		return education;
	}
	public void setWyksztalcenie(String wyksztalcenie) {
		this.education = wyksztalcenie;
	}
	public String getOpis() {
		return description;
	}
	public void setOpis(String opis) {
		this.description = opis;
	}
	
}
