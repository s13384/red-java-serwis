package main.java.rest.services;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import main.java.dao.MockRepositoryCatalog;
import main.java.dao.RepositoryCatalog;
import main.java.domain.Person;
import main.java.javafilters.AllFilters;
import main.java.rest.dto.PersonDto;
import main.java.rest.dto.PersonSummaryDto;

@Path("people")
public class ProfileService {

	RepositoryCatalog db = new MockRepositoryCatalog();

	@PUT
	@Path("/profileView")
	@Produces(MediaType.APPLICATION_JSON)
	public Person showProfile(PersonDto dto) {
		return db.people().get(dto.getId());
	}

	@PUT
	@Path("/profileEdit")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response profileEdit(Person p) {
		db.people().delete(db.people().get(p.getId()));
		db.people().add(p);

		return Response.status(201).build();
	}

	@PUT
	@Path("/search")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Person> search(ArrayList<Person> filters) {
		Person p1;
		Person p2;
		p1 = filters.get(0);
		p2 = filters.get(1);

		ArrayList<Person> allPeople;
		allPeople = (ArrayList<Person>) db.people();
		
		AllFilters.outFilter(allPeople, p1, p2);

		
		return allPeople;
	}

}
