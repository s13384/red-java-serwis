function EnumViewModel() {
    var self = this;

    // Non-editable catalog data - would come from the server
    self.avaliableEducations = [
        { education: "podstawowe"},
        { education: "srednie"},
        { education: "wyzsze"}
    ]; }

ko.applyBindings(new EnumViewModel());