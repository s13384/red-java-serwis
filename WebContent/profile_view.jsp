<%@ page language="java" contentType="text/html"
    pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates" %>    
<t:layout>
	<jsp:attribute name="styles">
		<!-- put your styles here -->
	</jsp:attribute>
	<jsp:attribute name="scripts">
		<script type="text/javascript" src="scripts/person/PersonViewModel.js"></script>
		<script type="text/javascript" src="scripts/person/PersonListViewModel.js"></script>
		<script type="text/javascript">
		$(function(){
			$.ajax({
	            url: "http://localhost:8080/servletjspdemo/rest/people/all",
	            type: "GET",
	            contentType: "application/json",
	            success: function (data) {
	                var viewModel = new PersonListViewModel(data);
	                ko.applyBindings(viewModel);
	            },
	            error: function (XMLHttpRequest, testStatus, errorThrown) {
	               alert("nie uda�o si�")

	            }
	        });
		});
		</script>
	
	</jsp:attribute>
	<jsp:body>
		<section id="body">
		<div class="container">
			<div class="yui3-g">
				<div class="yui3-u-1">
					<div id="profile-name">
						<h1>Nazwa profilu</h1>
					</div>
				</div>
			
				<div class="yui3-u-1-4 sidebar-left">
					<img class="img-responsive" alt="zdjęcie" src="http://placehold.it/150x200">
					<div class="sidebar-info">
						<dl>
							<dt>Wiek:</dt>
							<dd><strong data-bind="text: age"></strong></dd>
							<dt>Wzrost:</dt>
							<dd><strong data-bind="text: height"></strong></dd>
							<dt>Kolor oczu:</dt>
							<dd><strong data-bind="text: eyeColour"></strong></dd>
							<dt>Kolor włosów:</dt>
							<dd><strong data-bind="text: hairColour"></strong></dd>
							<dt>Wykształcenie:</dt>
							<dd><strong data-bind="text: education"></strong></dd>
						</dl>
					</div>
				</div>
				
				<div class="yui3-u-3-4 sidebar-right">
					<p><strong data-bind="text: description"></strong></p>
					<h2>Kogo szukam?</h2>
					<p><strong data-bind="text: description"></strong></p>
				</div>

			</div>
		</div>
	</section>
	</jsp:body>
	
</t:layout>