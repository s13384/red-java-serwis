<%@ page language="java" contentType="text/html"
    pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates" %>    
<t:layout>
	<jsp:attribute name="styles">
		<!-- put your styles here -->
	</jsp:attribute>
	<jsp:attribute name="scripts">
		<script type="text/javascript" src="scripts/person/PersonViewModel.js"></script>
		<script type="text/javascript" src="scripts/person/PersonListViewModel.js"></script>
		<script type="text/javascript">
		$(function(){
			$.ajax({
	            url: "http://localhost:8080/servletjspdemo/rest/people/all",
	            type: "GET",
	            contentType: "application/json",
	            success: function (data) {
	                var viewModel = new PersonListViewModel(data);
	                ko.applyBindings(viewModel);
	            },
	            error: function (XMLHttpRequest, testStatus, errorThrown) {
	               alert("nie uda�o si�")

	            }
	        });
		});
		</script>
	
	</jsp:attribute>
	<jsp:body>
		<section id="body">
		<div class="container">
			<form class="form-horizontal">
				<div class="yui3-g">

					<div class="yui3-u-1">
					
						<label class="title" for="age">Wiek:</label>
						<div class="input-box">
							<input class="input-field" id="age" data-bind="value: age" />
						</div>

						<label class="title" for="weight">Waga:</label>
						<div class="input-box">
							<input class="input-field" id="weight" name="age" data-bind="value: weight"/>
						</div>

						<label class="title" for="eye_color">Kolor oczu:</label>
						<div class="input-box">
							<input class="input-field" id="eye_color" name="age" data-bind="value: eyeColour"/>
						</div>

						<label class="title" for="residence">Zamieszkanie:</label>
						<div class="input-box">
							<input class="input-field" id="residence" name="age" data-bind="value: residence"/>
						</div>

						<label class="title" for="residence">Wykszta�cenie:</label>
						<div class="input-box">
							<select data-bind="options: $root.avaliableEducations, value: education, optionsText: 'education'">
							</select>
						</div>
						
						<label class="title" for="description">Opis:</label>
						<div class="input-box">
							<input class="text-field" id="description" name="description" data-bind="value: description"/>
						</div>
					</div>
				</div>
			</form>
		</div>
	</section>
	</jsp:body>
	
</t:layout>