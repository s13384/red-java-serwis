<%@ tag language="java" pageEncoding="UTF-8"%>
<%@attribute name="styles" fragment="true"%>
<%@attribute name="header" fragment="true"%>
<%@attribute name="scripts" fragment="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta charset="UTF-8">
<title>RedJava.pl</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link rel="stylesheet" type="text/css" href="styles/styles.css">
<link rel="stylesheet" type="text/css"
	href="http://yui.yahooapis.com/3.18.1/build/cssgrids/cssgrids-min.css">
</head>
<body>
	<div id="pageheader">
	<div class="container">
		<div class="logo">
			<a href="#">Moj profil</a>
		</div>
		<ul class="menu">
			<li><a href="#">Znajomi</a></li>
			<li><a href="#">Odwiedzający</a></li>
			<li><a href="#">Wyloguj</a></li>
		</ul>
		<div class="search-pos">
			<form class="search" role="search">
				<button type="submit" class="search-button">Szukaj</button>
				<div class="search-bar">
					<input type="text" class="search-field" placeholder="szukaj...">
				</div>
			</form>
		</div>
	</div>
	</div>
	<div id="content">
		<jsp:doBody />
	</div>
	<div id="footer">
		<script type="text/javascript" src="scripts/jquery-1.11.3.min.js"></script>
		<script type="text/javascript" src="scripts/knockout-3.3.0.js"></script>
		<script type="text/javascript" src="scripts/knockout.mapping.js"></script>

		<jsp:invoke fragment="scripts" />
	</div>
</body>
</html>