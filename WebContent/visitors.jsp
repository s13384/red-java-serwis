<%@ page language="java" contentType="text/html"
    pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates" %>    
<t:layout>
	<jsp:attribute name="styles">
		<!-- put your styles here -->
	</jsp:attribute>
	<jsp:attribute name="scripts">
		<script type="text/javascript" src="scripts/person/PersonViewModel.js"></script>
		<script type="text/javascript" src="scripts/person/PersonListViewModel.js"></script>
		<script type="text/javascript">
		$(function(){
			$.ajax({
	            url: "http://localhost:8080/servletjspdemo/rest/people/all",
	            type: "GET",
	            contentType: "application/json",
	            success: function (data) {
	                var viewModel = new PersonListViewModel(data);
	                ko.applyBindings(viewModel);
	            },
	            error: function (XMLHttpRequest, testStatus, errorThrown) {
	               alert("nie uda�o si�")

	            }
	        });
		});
		</script>
	
	</jsp:attribute>
	<jsp:body>
		Wszystkie osoby:<br/>
		<section id="body">
		<div class="container">
			<div class="yui3-g">
				<div class="media-box">
					<div class="yui3-u-1-6">
						<a href="#">
							<img class="media-object" alt="zdjęcie" src="http://placehold.it/150x200">
						</a>
					</div>
					<div class="yui3-u-5-6">
						<h4 class="media-title">Imie Nazwisko</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sed diam enim. Mauris aliquam luctus augue, sed rutrum dolor lobortis ac. Proin sollicitudin , eget sodales tellus varius. Pellentesque iaculis, massa varius vestibulum semper, mi arcu volutpat eros, et congue ex elit in est. Vestibulum ullamcorper dui id mauris aliquam, at pellentesque tellus tempor. Pellentesque habitant morbi tristique senectus et netus et malesuada fait aliquam eleollicitudin finibus. Phasellus at interdum augue, at tincidunt metus.</p>
					</div>
				</div>
				
				<div class="media-box">
					<div class="yui3-u-1-6">
						<a href="#">
							<img class="media-object" alt="zdjęcie" src="http://placehold.it/150x200">
						</a>
					</div>
					<div class="yui3-u-5-6">
						<h4 class="media-title">Imie Nazwisko</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sed diam enim. Mauris aliquam luctus augue, sed rutrum dolor lobortis ac. Proin sollicitudin , eget sodales tellus varius. Pellentesque iaculis, massa varius vestibulum semper, mi arcu volutpat eros, et congue ex elit in est. Vestibulum ullamcorper dui id mauris aliquam, at pellentesque tellus tempor. Pellentesque habitant morbi tristique senectus et netus et malesuada fait aliquam eleollicitudin finibus. Phasellus at interdum augue, at tincidunt metus.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	</jsp:body>
	
</t:layout>